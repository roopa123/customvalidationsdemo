<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\validationDemoRepository;

class validationDemoController extends Controller
{

	private $validationRepo;

	function __construct(validationDemoRepository $validationRepo){
		$this->validationRepo = $validationRepo;
	}
	//return view
    public function index()
    {
    	return view('validations.create');
    }

    /**
    * Store emp details
    *@param Request $request
    *@return view
    */
    public function store(\Request $request)
    {
    	$validator = \Validator::make(request()->all(), $this->validationRepo->validationRules);
    	dd($validator->errors());
            if ($validator->fails()) {
                return $validator->errors()->getMessages();
            }
        return "true";
    }
}
