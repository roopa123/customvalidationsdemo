<?php
namespace App\Repositories;

class validationDemoRepository 
{
     
    /**
    * Validation for assigning people to project and manager
    */
    public $validationRules = [
        'name' => 'required|unique_version_for_appcode',
        'email' => 'required',
        'designation' => 'required',
        'salary' => 'required',
    ];

}