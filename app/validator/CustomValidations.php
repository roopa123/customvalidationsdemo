<?php
namespace App\validator;

use Illuminate\Validation\Validator as IlluminateValidator;

class CustomValidations extends IlluminateValidator
{
	private $_custom_messages = array(
		'unique_version_for_appcode' => 'Combination of App and Version already exists'
		);
    
    public function __construct( $translator, $data, $rules, $messages = array(), $customAttributes = array()) {

        parent::__construct($translator, $data, $rules, $messages, $customAttributes);
        //$this->_set_custom_stuff();        
    }

    /**
     * Setup any customizations etc
     *
     * @return void
     */
    /*protected function _set_custom_stuff($messages=array()) {
        //setup our custom error messages
        $this->setCustomMessages( $this->_custom_messages + $messages);
    }*/

    /********************Release Rollout********************************/

    /**
     * Validate unique version for app
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  array   $parameters
     * @return bool
     */
    public function validateUniqueVersionForAppCode($attribute, $value, $parameters)
    {
       	dd($value);        
    }

}
