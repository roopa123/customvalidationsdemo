<?php
namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;

class CustomValidationServiceProvider extends ServiceProvider {

    public function register() {

    }

    public function boot() {  
        /**
        * Validator for Host Vmc
        */
        $this->app->validator->resolver(function( $translator, $data, $rules, $messages = array(), $customAttributes = array()) 
        {
            return new \App\validator\CustomValidations($translator, $data, $rules, $messages, $customAttributes);
        });       
    }

}
